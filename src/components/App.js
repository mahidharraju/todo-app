import React, { Component } from "react";

import Home from "./pages/Home/Home";

class App extends Component {
  render() {
    return (
      <div>
        <Home></Home>
      </div>
    );
  }
}

export default App;
