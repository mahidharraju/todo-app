import React, { useContext } from "react";
import CheckBox from "../../atoms/Checkbox/CheckBox";
import Button from "../../atoms/Button/Button";
import { TodoListContext } from "../../../context/TodoListContext";

const ToDoItem = (props) => {
  const { removeTodo, findTodoById, updateTodoStatus } = useContext(
    TodoListContext
  );

  return (
    <li className="list-item">
      <span>
        <CheckBox item={props.todoItem} event={updateTodoStatus}></CheckBox>
      </span>
      <div>
        <Button
          name={props.editButton}
          type="button"
          handleEvent={() => findTodoById(props.todoItem.id)}
          status={props.todoItem.status}
        ></Button>

        <Button
          name={props.deleteButton}
          handleEvent={() => removeTodo(props.todoItem.id)}
          type="button"
          status={props.todoItem.status}
        ></Button>
      </div>
    </li>
  );
};

export default ToDoItem;
