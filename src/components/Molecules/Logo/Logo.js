import React from "react";
import PropTypes from "prop-types";
import Image from "../../atoms/Image/Image";

const Logo = (props) => <Image src={props.src} alt={props.alt} />;

Logo.prototype = {
  src: PropTypes.string,
  alt: PropTypes.string,
};

export default Logo;
