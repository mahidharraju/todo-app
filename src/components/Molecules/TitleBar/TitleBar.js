import React from "react";
import Title from "../../atoms/Title/Title";
import Image from "../../atoms/Image/Image";

const TitleBar = (props) => (
  <div className="header">
    <Title titleText={props.titleText} />
  </div>
);

export default TitleBar;
