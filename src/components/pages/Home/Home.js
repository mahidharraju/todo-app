import React, { Component, useContext } from "react";
import Header from "../../organisms/Header/Header";
import CreateTodo from "../../organisms/CreateTodoItem/CreateTodoItem";
import TodoList from "../../organisms/ToDoList/TodoList";
import TodoListContextProvider from "../../../context/TodoListContext";
import "../../../style.css";

const Home = () => {
  return (
    <div>
      <TodoListContextProvider>
        <div className="container">
          <div className="app-wrapper">
            <Header titleText="TODO" />
            <div className="main">
              <CreateTodo />
              <TodoList />
            </div>
          </div>
        </div>
      </TodoListContextProvider>
    </div>
  );
};

export default Home;
