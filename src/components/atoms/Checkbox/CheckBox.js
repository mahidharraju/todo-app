import React, { useContext } from "react";
import { TodoListContext } from "../../../context/TodoListContext";

const CheckBox = (props) => {
  const { updateTodoStatus } = useContext(TodoListContext);
  return (
    <div>
      <input
        type="checkbox"
        id={props.item.id}
        name={props.item.id}
        onChange={() => updateTodoStatus(props.item.id)}
      />
      <label style={{ margin: 10 }} htmlFor={props.item.id}>
        {props.item.status ? (
          <s>{props.item.description}</s>
        ) : (
          props.item.description
        )}
      </label>
    </div>
  );
};

export default CheckBox;
