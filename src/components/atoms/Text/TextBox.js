import React from "react";
import PropTypes from "prop-types";

const TextBox = (props) => (
  <React.Fragment>
    <input
      type={props.type}
      onChange={props.textChangeEvent}
      value={props.currentItem}
      name="create"
      className="task-input"
      placeholder="Add New Todo Item..."
      required
      autocomplete="off"
      onfocus="this.value = this.value;"
    ></input>
  </React.Fragment>
);

TextBox.propTypes = {
  type: PropTypes.string,
};
export default TextBox;
