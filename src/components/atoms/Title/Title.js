import React from "react";
import PropTypes from "prop-types";

const Title = (props) => <h1>{props.titleText}</h1>;

Title.propTypes = {
  titleText: PropTypes.string,
};

export default Title;
