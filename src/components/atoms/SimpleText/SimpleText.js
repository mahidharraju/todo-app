import React from "react";
import PropTypes from "prop-types";

const SimpleText = (props) => <h4>{props.taskDescription}</h4>;

SimpleText.propTypes = {
  taskDescription: PropTypes.string,
};

export default SimpleText;
