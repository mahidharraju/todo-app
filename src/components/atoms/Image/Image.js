import React from "react";
import PropTypes from "prop-types";

const Image = (props) => <img src={props.src} alt={props.alt}></img>;

Image.propTypes = {
  imageSource: PropTypes.string,
  alternate: PropTypes.string,
};

export default Image;
