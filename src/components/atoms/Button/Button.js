import React from "react";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";

import IconButton from "@material-ui/core/IconButton";
const Btn = (props) =>
  props.name === "Delete" ? (
    <IconButton
      aria-label="delete"
      onClick={props.handleEvent}
      disabled={props.status}
    >
      <DeleteIcon fontSize="small" />
    </IconButton>
  ) : props.name === "Edit" ? (
    <IconButton
      aria-label="delete"
      disabled={props.status}
      onClick={props.handleEvent}
      type="submit"
    >
      <EditIcon fontSize="small" />
    </IconButton>
  ) : (
    <Button
      disabled={props.status}
      type={props.type}
      onClick={props.handleEvent}
      variant="contained"
      color="primary"
      size="small"
      startIcon={<SaveIcon />}
    >
      {props.name}
    </Button>
  );

Btn.propTypes = {
  text: PropTypes.string,
};

export default Btn;
