import React from "react";
import PropTypes from "prop-types";
import TitleBar from "../../Molecules/TitleBar/TitleBar";

const Header = (props) => (
  <div>
    <TitleBar titleText={props.titleText} />
  </div>
);

export default Header;
