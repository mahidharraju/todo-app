import React, { useContext, useState, useEffect } from "react";
import Button from "../../atoms/Button/Button";
import TextBox from "../../atoms/Text/TextBox";
import { TodoListContext } from "../../../context/TodoListContext";

const CreateTodo = (props) => {
  const { addTodo, editTodo, editedTodo } = useContext(TodoListContext);
  const [todo, setTodo] = useState("");

  const textChangeEvent = (e) => {
    setTodo(e.target.value);
  };

  const handleCreateTodo = (e) => {
    e.preventDefault();
    if (editedTodo === null) {
      addTodo(todo);
      setTodo("");
    } else {
      editTodo(todo, editedTodo.id);
    }
  };

  useEffect(() => {
    if (editedTodo !== null) {
      setTodo(editedTodo.description);
    } else {
      setTodo("");
    }
  }, [editTodo]);

  return (
    <form onSubmit={handleCreateTodo}>
      <div className="form">
        <div style={{ margin: 10 }}>
          <TextBox
            type="text"
            textChangeEvent={textChangeEvent}
            currentItem={todo}
          />
        </div>

        <div className="buttons">
          <Button
            name={editedTodo ? "Edit Todo" : "Create Todo"}
            type="submit"
          />
        </div>
      </div>
    </form>
  );
};

export default CreateTodo;
