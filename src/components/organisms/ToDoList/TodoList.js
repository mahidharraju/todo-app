import React, { useContext } from "react";
import { TodoListContext } from "../../../context/TodoListContext";

import ToDoItem from "../../Molecules/ToDoItem/ToDoItem";

const TodoList = () => {
  const { todoList } = useContext(TodoListContext);
  return (
    <div className="list">
      {todoList.length ? (
        <ul>
          {todoList.map((todoItem) => {
            return (
              <ToDoItem
                key={todoItem.id}
                todoItem={todoItem}
                editButton="Edit"
                deleteButton="Delete"
              ></ToDoItem>
            );
          })}
        </ul>
      ) : (
        <div className="no-tasks">No tasks... </div>
      )}
    </div>
  );
};

export default TodoList;
