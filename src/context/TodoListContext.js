import React, { createContext, useState } from "react";
import uuid from "uuid";

export const TodoListContext = createContext();

const TodoListContextProvider = (props) => {
  const [todoList, setTodos] = useState([
    { id: 1, description: "Submit code", status: false },
    { id: 2, description: "Fix code review comments", status: false },
    { id: 3, description: "Clean room and Car", status: false },
  ]);

  const [editedTodo, setEditedTodo] = useState(null);

  const addTodo = (description) => {
    setTodos([...todoList, { description, id: uuid(), status: false }]);
  };
  const removeTodo = (id) => {
    setTodos(todoList.filter((todo) => todo.id !== id));
  };

  const findTodoById = (id) => {
    const item = todoList.find((todo) => todo.id === id);
    setEditedTodo(item);
  };

  const editTodo = (description, id) => {
    const newTodoList = todoList.map((todo) =>
      todo.id === id
        ? {
            id: id,
            description: description,
            status: false,
          }
        : todo
    );
    setTodos(newTodoList);
    setEditedTodo(null);
  };

  const updateTodoStatus = (id) => {
    const newTodoList = todoList.map((todo) =>
      todo.id === id
        ? {
            id: id,
            description: todo.description,
            status: todo.status ? false : true,
          }
        : todo
    );
    setTodos(newTodoList);
  };

  return (
    <TodoListContext.Provider
      value={{
        todoList,
        editedTodo,
        addTodo,
        removeTodo,
        findTodoById,
        editTodo,
        updateTodoStatus,
      }}
    >
      {props.children}
    </TodoListContext.Provider>
  );
};

export default TodoListContextProvider;
